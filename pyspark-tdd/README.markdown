# Mars Petcare: Pyspark interview question

## Setup to run locally
1. **Install miniconda**  
    a. Here (https://docs.conda.io/en/latest/miniconda.html#) is a list of installers for each operating system. Download and install the one you're on. Note:  
        i. If you're using Windows make sure you choose the "Install for Just Me" option.  
        ii. You want to add conda to PATH variables for easier use later.  
    b. After the installation is successful you can test this by running `conda` in a terminal.  
2. **Local setup**  
    a. Create a new conda environment and activate it. Allow all new packages to be installed. This will setup a new development  
        `conda create -n <your-environment-name> python=3.9`  
        `conda activate <your-environment-name>`  
    b.  Install project requirements  
        `pip install -r requirements.txt`  
    c.  Install java8 in your conda environment
        `conda install -c anaconda "openjdk=8.0.152"`


##Task
Clone test package from https://github.com/richard-nixon1/pyspark-tdd-interview.git

Implement the following funcions in project.py:
- get_distinct_count
- sum_of_column
- rename_column
- add_literal_string_column

##Prerequisites
install required packages:
- atomicwrites==1.4.0
- attrs==21.2.0
- colorama==0.4.4
- iniconfig==1.1.1
- packaging==20.9
- pluggy==0.13.1
- py==1.10.0
- py4j==0.10.9
- pyparsing==2.4.7
- pyspark==3.1.1
- pytest==6.2.4
- toml==0.10.2

##Test
Run test (pytest test_project.py)

##Update 29/01/2024
Last test failed becuse of bug in the function - add_literal_string_column. 

- Fixed the code - by removing hard coded values for new_col and col_value.
