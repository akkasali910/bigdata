import os
import sys

os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

# CODE BELOW

import pyspark.sql.functions as SF

# functions

def get_distinct_count(df):
  """
  distinct count from the dataframe and return count
  """
  distinct_count = df.distinct().count()
  return distinct_count

def sum_of_column(df,col_name):
  """
  get sum for as given column
  """
  sum = df.agg(SF.sum(col_name)).collect()[0][0]
  return sum

def rename_column(df,old_col,new_col):
  """
  given a dataframe with old and new col 
  return dataframe with new col
  """

  df = df.withColumnRenamed(old_col,new_col)
  return df

def add_literal_string_column(df,new_col,col_value):
  """
  add literal string to column  - creating a new column assgining value
  """
  df = df.withColumn(new_col,SF.lit(col_value))
  return df  





