### Changes to codes 
You need to make changes to: 
1. server.py 
2. Create a new class to read data from sample files (I called it ElectionResult) 
3. Update results_controller.py and add your logic to complete the task.

### Contents of file_service.py
```
import json

class ElectionResult():
  """Read election result files and return result"""  

  def __init__(self,file_dir) -> None:
    self.file_dir = file_dir

  def load_and_post_result_file(self, num: str) -> dict:
    file_number: str = str(num).zfill(3)
    with open(f"{self.file_dir}/result{file_number}.json", "r") as file:
      result = file.read()
    return json.loads(result)

  def load_results(self,quantity: int) -> list[dict]:
    results = []
    for i in range(quantity):
      results.append(self.load_and_post_result_file(i + 1))
    return results
```

### update server.py
```
import os
#file_dir: str = os.path.dirname(os.path.realpath(__name__))
file_dir = "/Users/aliakkas/apps/ons/software-engineering-technical-assessments/election-api-python/src"    
RESULT_PATH: str = f"{file_dir}/resources/sample-election-results"
electionresult = ElectionResult(RESULT_PATH)

@app.route("/result", methods=["POST"])
def add_result() -> dict:
    id = request.json['file_no']
    if not id:
      data = request.json
    else:
      data = electionresult.load_and_post_result_file(id)
    return controller.new_result(data)

```

### implement your code - results_controller.py
```
    def scoreboard(self) -> dict:
        # Left blank for you to fill in
        result = self.store.get_all()
        return result[0]
```

### Test your code
Testing Election Result Application
1. run the main.py  ( python ./src/main.py)

On a separte window or using POSTMAN  
2. send post request to server. (127.0.0.1:3000/result)

curl -X POST -H "Content-Type: application/json" -d '{ "file_no": 2}' http://127.0.0.1:3000/result

To check output
curl http://127.0.0.1:3000/result/16 (id no see sample file)
 

