import json

class ElectionResult():
  """Read election result files and return result"""  

  def __init__(self,file_dir) -> None:
    self.file_dir = file_dir

  def load_and_post_result_file(self, num: str) -> dict:
    file_number: str = str(num).zfill(3)
    with open(f"{self.file_dir}/result{file_number}.json", "r") as file:
      result = file.read()
    return json.loads(result)

  def load_results(self,quantity: int) -> list[dict]:
    results = []
    for i in range(quantity):
      results.append(self.load_and_post_result_file(i + 1))
    return results
