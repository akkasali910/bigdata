from flask import Flask, request
from results_controller import ResultsController
from file_service import ElectionResult

app: Flask = Flask(__name__)

import os
#file_dir: str = os.path.dirname(os.path.realpath(__name__))
file_dir = "/Users/aliakkas/apps/ons/software-engineering-technical-assessments/election-api-python/src"    
RESULT_PATH: str = f"{file_dir}/resources/sample-election-results"
electionresult = ElectionResult(RESULT_PATH)

controller: ResultsController = ResultsController()

@app.route("/result/<id>", methods=["GET"])
def individual_result(id) -> dict:
    return controller.get_result(int(id))

@app.route("/result", methods=["POST"])
def add_result() -> dict:
    id = request.json['file_no']
    if not id:
      data = request.json
    else:
      data = electionresult.load_and_post_result_file(id)
    return controller.new_result(data)

@app.route("/scoreboard", methods=["GET"])
def scoreboard() -> dict:
    return controller.scoreboard()
    
